<div id="content">
	<div class="row">
		<div class="col-md-6">
			<h1>Tech Savy</h1>
			<div class="sub">Make a statement on the web with your online presence. Get noticed, be professional, and let TechSavy personally design a website to reach people and take you to the next level.</div>
			<p>Here at Tech Savy, we not only want to help you Savy On The Savvy; we want to get to know you, your vision, goals and desires in order to make a
			   website that clearly reflects at the highest level. We also want to enlighten your understanding of how we can help you in other ways.
			</p>
			<a href="<?php echo URL; ?>about" class="btn">Explore Tech Savy Now</a>
		</div>
		<div class="col-md-6 img-holder">
			<img src="public/images/content/content-img1.jpg" alt="">
			<div class="blue-line"></div>
		</div>
	</div>
</div>
<div class="slogan">
	<div class="row">
		<h3>Savy On The Savvy</h3>
	</div>
</div>
<div id="goal">
	<div class="row">
		<div class="col-md-6">
			<img src="public/images/content/goal-img1.jpg" alt="">
		</div>
		<div class="col-md-6">
			<div class="goal-text">
				<h2>Our only goal is to make you and your company look really good.</h2>
				<p>We listen to what you are saying...and we listen to what you’re not saying. Because sometimes you’re not sure what you really want, and we specialize in helping you figure it out.</p>
				<p>Then we brainstorm and research and come up with a unique solution or product that will make your competitors weep with envy.</p>
			</div>
		</div>
	</div>
</div>
<div id="services">
	<div class="row">
		<img src="public/images/content/serv-img1.jpg" alt="" class="serv-img">
		<div class="text-on-img">
			Design,<br>Development,<br>Marketing
		</div>
	</div>
</div>

<!--<div id="services2">
	<div class="row">
		<div class="col-md-12 left">
			<h2>ABOUT US</h2>
			<div class="lorem">Tell about your goals! And we'll help you capitalize on organic search results, professional layouts, thousands of customizable options, responsive websites and personable service.</div>
			<ul>
				<li>Web Design</li>
				<li>Search Engine Optimization</li>
				<li>Graphic Design</li>
				<li>Logo Design</li>
				<li>Custom Web Design</li>
				<li>Programming</li>
				<li>Design Only</li>
				<li>Domain Hosting</li>
				<li>Layout Design</li>
			</ul>
		</div>-->
		<!-- <div class="col-md-5 right">
			<div class="we-do">What we can do</div>
			<div class="we-do-after">
				<ul>
					<li class="pro">PROFESSIONAL</li>
					<li class="hr"></li>
					<li class="fri">FRIENDLY SERVICE</li>
					<li class="hr"></li>
					<li class="hon">HONEST & RELIABLE</li>
				</ul>
			</div>
		</div>
	</div>
</div>-->

<div id="midsection">
	<div class="row">
		<div class="content-holder">Does design matter? <span>How important is design to business.</span></div>
		<img src="public/images/content/design-img1.png" alt="" class="design-img">
		<div class="blue-line"></div>
	</div>
</div>

<div id="top-bottom">
	<div class="row">
		<div class="col-md-6">
			<div class="text">
				<h3>History of Innovation and Experience</h3>
				<p>We design and build websites for small and medium-sized businesses that look good on all devices. We custom-create your site so it’s easy to navigate by your customers and easy for you to update.</p>
				<p>We combine great photos, creative content and all the “behind-the-scenes” details that keep your site looking and working great. We also focus much of our attention on SEO (Search Engine Optimization), a process that makes sure your site is found on search engines like Google.</p>
				<a href="<?php echo URL; ?>about" class="btn">SHARE your Goals!</a>
			</div>
		</div>
		<div class="col-md-6">&nbsp;</div>
		<img src="public/images/content/top-bottom-img1.jpg" alt="" class="top-bottom-img">
	</div>
</div>
