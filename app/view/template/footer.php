<footer>
	<div id="footer">
		<div class="row">
			<img src="public/images/content/logo-ft.png" alt="" class="logo-ft">
		<hr>
		<div class="col-md-3">
			<div class="caption">About Us</div>
			<div class="lorem">
				We take web designing seriously, from the builder’s basics to highly advanced training and software; our team is ready to help you go beyond what you can actually see.
			</div>
		</div>
		<div class="col-md-3">
			<div class="caption">Services</div>
			<ul class="list">
				<li>Web Design</li>
				<li>Logo Design</li>
				<li>Search Engine Optimization</li>
				<li>Domain Hosting</li>
			</ul>
		</div>
		<div class="col-md-3">
			<div class="caption">Navigation</div>
			<ul class="list">
				<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home</a></li>
				<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about">About Us</a></li>
				<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">Gallery</a></li>
				<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">Contact Us</a></li>
			</ul>
		</div>
		<!-- <div class="col-md-3">
			<div class="caption">Contact Us</div>
			<div class="info"><span class="sub">Email</span> <?php $this->info(["email","mailto"]); ?></div>
			<div class="info"><span class="sub">Address</span> <?php $this->info("address"); ?></div>
			<div class="info"><span class="sub">Phone</span> <?php $this->info(["phone","tel"]); ?></div>
			<div class="info"><span class="sub">Alt</span> <?php $this->info(["alt","tel"]); ?></div>
			<div class="info"><span class="sub">Fax</span> <?php $this->info(["fax","tel"]); ?></div>
		</div> -->
		<div class="col-md-3">
			<div class="caption">Send a Message!</div>
			<!-- <div class="info"><span class="sub">Email</span> <?php $this->info(["email","mailto"]); ?></div> -->
			<!-- <div class="info"><span class="sub">Address</span> <?php $this->info("address"); ?></div> -->
			<div class="info"><!-- <span class="sub">Phone</span> --> <?php $this->info(["phone","tel"]); ?></div>
			<div class="info"><!-- <span class="sub">Alt</span> --> <?php $this->info(["alt","tel"]); ?></div>
			<!-- <div class="info"><span class="sub">Fax</span> <?php $this->info(["fax","tel"]); ?></div> -->
		</div>
		<p class="dis">
			The registered names are the properties of the respective owners.
			<br>
			<br>
			Disclaimer: On this site any mention of commercial products or reference to commercial organizations is for information only; it does not imply recommendation or endorsement by the Tech Savy nor does it imply that the products mentioned are necessarily the best available for the purpose.
			</p>
			<p class="copy">
				2007-<?php echo date("Y"); ?> Copyright <?php $this->info("company_name"); ?>, All rights reserved
			</p>
			<!--<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>-->
		</div>
	</div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script type="text/javascript" src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
			});
		};
	</script>
<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>
